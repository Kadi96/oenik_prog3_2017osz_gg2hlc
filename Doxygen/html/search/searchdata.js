var indexSectionsWithContent =
{
  0: "abcdefgiklmnoprstvxy",
  1: "abcegilmopstvy",
  2: "kx",
  3: "abcefgilmopsv",
  4: "bev",
  5: "abcdefilmnpsvxy",
  6: "g",
  7: "r"
};

var indexSectionNames =
{
  0: "all",
  1: "classes",
  2: "namespaces",
  3: "functions",
  4: "variables",
  5: "properties",
  6: "events",
  7: "pages"
};

var indexSectionLabels =
{
  0: "All",
  1: "Classes",
  2: "Namespaces",
  3: "Functions",
  4: "Variables",
  5: "Properties",
  6: "Events",
  7: "Pages"
};

