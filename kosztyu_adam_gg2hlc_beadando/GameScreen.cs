﻿namespace Kosztyu_adam_gg2hlc_beadando
{
    using System;
    using System.Collections.Generic;
    using System.Globalization;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using System.Windows;
    using System.Windows.Controls;
    using System.Windows.Input;
    using System.Windows.Media;
    using System.Windows.Media.Imaging;
    using System.Windows.Threading;

    /// <summary>
    /// Game screen class.
    /// </summary>
    public class GameScreen : FrameworkElement
    {
        /// <summary>
        /// Timer variable.
        /// </summary>
        private DispatcherTimer dt;

        /// <summary>
        /// View Model variable.
        /// </summary>
        private ViewModel vm;

        /// <summary>
        /// Logic variable.
        /// </summary>
        private GameLogic logic;

        /// <summary>
        /// Current window variable.
        /// </summary>
        private Window gameWin;

        /// <summary>
        /// Big laser counter to Player one.
        /// </summary>
        private int megaLaserP1 = 5;

        /// <summary>
        /// Big laser counter to player two.
        /// </summary>
        private int megaLaserP2 = 5;

        /// <summary>
        /// Shield life to player one.
        /// </summary>
        private int shieldP1 = 2;

        /// <summary>
        /// Shield life to player two.
        /// </summary>
        private int shieldP2 = 2;

        /// <summary>
        /// Player one death logic.
        /// </summary>
        private bool player1Death = false;

        /// <summary>
        /// Player two death logic.
        /// </summary>
        private bool player2Death = false;

        /// <summary>
        /// List of the pressed keys because of the multiplayer.
        /// </summary>
        private List<Key> pressedKeys;

        /// <summary>
        /// Constructor of the Game screen class.
        /// Initializes a new instance of the <see cref="GameScreen"/> class.
        /// </summary>
        public GameScreen()       
        {
            this.vm = ViewModel.Get();
            this.logic = new GameLogic();
            this.dt = new DispatcherTimer() { Interval = TimeSpan.FromMilliseconds(30) };
            this.dt.Tick += this.Dt_Tick;
            this.logic.VM = this.vm;
            this.Loaded += this.Screen_Loaded;
            this.logic.GameOver += this.Logic_GameOver;
            this.logic.GameWin += this.Logic_GameWin;
            this.pressedKeys = new List<Key>();         
        }

        /// <summary>
        /// Override On Render method to draw every components of the game.
        /// </summary>
        /// <param name="drawingContext">Drawing context parameter.</param>
        protected override void OnRender(DrawingContext drawingContext)
        {
            foreach (Stars item in this.vm.Stars)
            {
                drawingContext.DrawImage(item.ComponentImage, item.Shape.Bounds);
            }

            if (this.logic.ShieldOnP1 == true)
            {
                drawingContext.DrawImage(this.vm.Player1.ComponentImage, this.vm.Player1.Shape.Bounds);
            }
            else
            {
                drawingContext.DrawImage(this.vm.Player1.ComponentImage, this.vm.Player1.Shape.Bounds);
            }

            if (this.vm.Player2 != null)
            {
                if (this.logic.ShieldOnP2 == true)
                {
                    drawingContext.DrawImage(this.vm.Player2.ComponentImage, this.vm.Player2.Shape.Bounds);
                }
                else
                {
                    drawingContext.DrawImage(this.vm.Player2.ComponentImage, this.vm.Player2.Shape.Bounds);
                }
            }

            foreach (Laser item in this.vm.Laser)
            {
                drawingContext.DrawImage(item.ComponentImage, item.Shape.Bounds);
            }

            foreach (BigLaser item in this.vm.Biglaser)
            {
                drawingContext.DrawImage(item.ComponentImage, item.Shape.Bounds);
            }

            foreach (Asteroids item in this.vm.Asteroids)
            {
                drawingContext.DrawImage(item.ComponentImage, item.Shape.Bounds);
            }

            foreach (EnemySpaceship item in this.vm.Enemyships)
            {
                drawingContext.DrawImage(item.ComponentImage, item.Shape.Bounds);
            }

            foreach (EnemyLaser item in this.vm.Enemylaser)
            {
                drawingContext.DrawImage(item.ComponentImage, item.Shape.Bounds);
            }

            foreach (BossEnergyBall item in this.vm.Bossenergyball)
            {
                drawingContext.DrawImage(item.ComponentImage, item.Shape.Bounds);
            }

            foreach (BossLaser item in this.vm.Bosslaser)
            {
                drawingContext.DrawImage(item.ComponentImage, item.Shape.Bounds);
            }

            drawingContext.DrawImage(this.vm.FinalBoss.ComponentImage, this.vm.FinalBoss.Shape.Bounds);

            if (this.vm.FinalBoss.Life <= 10)
            {
                this.vm.FinalBoss.ComponentImage = new BitmapImage(new Uri(@"pack://application:,,,/Resources/ExplodedDeathStar.png"));
                drawingContext.DrawImage(this.vm.FinalBoss.ComponentImage, this.vm.FinalBoss.Shape.Bounds);
            }

            if (this.vm.Player2 != null)
            {
                drawingContext.DrawText(new FormattedText(this.vm.Player1.Name + " és " + this.vm.Player2.Name + " Pontszáma: " + this.logic.PlayerPoints, CultureInfo.CurrentCulture, FlowDirection.LeftToRight, new Typeface("Arial Black"), 12, Brushes.Yellow), new Point(15, 780));
                drawingContext.DrawText(new FormattedText("Pajzs: " + this.shieldP1, CultureInfo.CurrentCulture, FlowDirection.LeftToRight, new Typeface("Arial Black"), 12, Brushes.Yellow), new Point(15, 700));
                drawingContext.DrawText(new FormattedText("Mega lézer: " + this.megaLaserP1, CultureInfo.CurrentCulture, FlowDirection.LeftToRight, new Typeface("Arial Black"), 12, Brushes.Yellow), new Point(15, 720));
                drawingContext.DrawText(new FormattedText("Pajzs: " + this.shieldP2, CultureInfo.CurrentCulture, FlowDirection.LeftToRight, new Typeface("Arial Black"), 12, Brushes.Yellow), new Point(500, 700));
                drawingContext.DrawText(new FormattedText("Mega lézer: " + this.megaLaserP2, CultureInfo.CurrentCulture, FlowDirection.LeftToRight, new Typeface("Arial Black"), 12, Brushes.Yellow), new Point(500, 720));

                if (this.vm.Player1.Life == 3)
                {
                    drawingContext.DrawImage(this.vm.Player1.LifeImage, new Rect(15, 740, 30, 30));
                    drawingContext.DrawImage(this.vm.Player1.LifeImage, new Rect(45, 740, 30, 30));
                    drawingContext.DrawImage(this.vm.Player1.LifeImage, new Rect(75, 740, 30, 30));
                }

                if (this.vm.Player1.Life == 2)
                {
                    drawingContext.DrawImage(this.vm.Player1.LifeImage, new Rect(15, 740, 30, 30));
                    drawingContext.DrawImage(this.vm.Player1.LifeImage, new Rect(45, 740, 30, 30));
                }

                if (this.vm.Player1.Life == 1)
                {
                    drawingContext.DrawImage(this.vm.Player1.LifeImage, new Rect(15, 740, 30, 30));
                }

                if (this.vm.Player1.Life == 0)
                {
                    this.vm.Player1.Shape = new EllipseGeometry(new Rect(-100, -100, 0, 0));
                    drawingContext.DrawImage(this.vm.Player1.ComponentImage, this.vm.Player1.Shape.Bounds);
                    drawingContext.DrawImage(this.vm.Player1.LifeImage, new Rect(0, 0, 0, 0));
                    this.player1Death = true;
                }

                if (this.vm.Player2.Life == 3)
                {
                    drawingContext.DrawImage(this.vm.Player2.LifeImage, new Rect(500, 740, 30, 30));
                    drawingContext.DrawImage(this.vm.Player2.LifeImage, new Rect(530, 740, 30, 30));
                    drawingContext.DrawImage(this.vm.Player2.LifeImage, new Rect(560, 740, 30, 30));
                }

                if (this.vm.Player2.Life == 2)
                {
                    drawingContext.DrawImage(this.vm.Player2.LifeImage, new Rect(500, 740, 30, 30));
                    drawingContext.DrawImage(this.vm.Player2.LifeImage, new Rect(530, 740, 30, 30));
                }

                if (this.vm.Player2.Life == 1)
                {
                    drawingContext.DrawImage(this.vm.Player2.LifeImage, new Rect(500, 740, 30, 30));
                }

                if (this.vm.Player2.Life == 0)
                {
                    this.vm.Player2.Shape = new EllipseGeometry(new Rect(-100, -100, 0, 0));
                    drawingContext.DrawImage(this.vm.Player2.ComponentImage, this.vm.Player2.Shape.Bounds);
                    drawingContext.DrawImage(this.vm.Player2.LifeImage, new Rect(0, 0, 0, 0));
                    this.player2Death = true;
                }
            }
            else
            {
                drawingContext.DrawText(new FormattedText(this.vm.Player1.Name + " Pontszáma:" + this.logic.PlayerPoints, CultureInfo.CurrentCulture, FlowDirection.LeftToRight, new Typeface("Arial Black"), 12, Brushes.Yellow), new Point(15, 680));
                drawingContext.DrawText(new FormattedText("Pajzs: " + this.shieldP1, CultureInfo.CurrentCulture, FlowDirection.LeftToRight, new Typeface("Arial Black"), 12, Brushes.Yellow), new Point(15, 700));
                drawingContext.DrawText(new FormattedText("Mega lézer: " + this.megaLaserP1, CultureInfo.CurrentCulture, FlowDirection.LeftToRight, new Typeface("Arial Black"), 12, Brushes.Yellow), new Point(15, 720));
                if (this.vm.Player1.Life == 3)
                {
                    drawingContext.DrawImage(this.vm.Player1.LifeImage, new Rect(15, 740, 30, 30));
                    drawingContext.DrawImage(this.vm.Player1.LifeImage, new Rect(45, 740, 30, 30));
                    drawingContext.DrawImage(this.vm.Player1.LifeImage, new Rect(75, 740, 30, 30));
                }

                if (this.vm.Player1.Life == 2)
                {
                    drawingContext.DrawImage(this.vm.Player1.LifeImage, new Rect(15, 740, 30, 30));
                    drawingContext.DrawImage(this.vm.Player1.LifeImage, new Rect(45, 740, 30, 30));
                }

                if (this.vm.Player1.Life == 1)
                {
                    drawingContext.DrawImage(this.vm.Player1.LifeImage, new Rect(15, 740, 30, 30));
                }
            }
        }

        /// <summary>
        /// Game Exit event's Logic.
        /// </summary>
        /// <param name="sender">Sender object.</param>
        /// <param name="e">Event parameter.</param>
        private void Logic_GameExit(object sender, EventArgs e)
        {
            this.dt.Stop();
            this.gameWin.Close(); 
        }

        /// <summary>
        /// Game Win event's logic.
        /// </summary>
        /// <param name="sender">Sender object.</param>
        /// <param name="e">Event parameter.</param>
        private void Logic_GameWin(object sender, EventArgs e)
        {
            this.dt.Stop();
            YouWinWindow youWin;
            if (this.vm.Player2 != null)
            {
                youWin = new YouWinWindow(this.logic.PlayerPoints, (this.vm.Player1.Life + this.vm.Player2.Life) / 2, this.vm.Player1.Name + " és " + this.vm.Player2.Name);
                youWin.ShowDialog();
                this.gameWin.Close();   
            }
            else
            {
                youWin = new YouWinWindow(this.logic.PlayerPoints, this.vm.Player1.Life, this.vm.Player1.Name);
                youWin.ShowDialog();
                this.gameWin.Close();      
            }             
        }

        /// <summary>
        /// Game over event's logic.
        /// </summary>
        /// <param name="sender">Sender object.</param>
        /// <param name="e">Event parameter.</param>
        private void Logic_GameOver(object sender, EventArgs e)
        {
            this.dt.Stop();
            GameOverWindow gameOver = new GameOverWindow();
            gameOver.ShowDialog();
            this.gameWin.Close();          
        }

        /// <summary>
        /// To Update the game screen.
        /// </summary>
        /// <param name="sender">Sender object.</param>
        /// <param name="e">Event parameter.</param>
        private void Dt_Tick(object sender, EventArgs e)
        {
            this.InvalidateVisual();       
        }

        /// <summary>
        /// Set the background and give the key events.
        /// </summary>
        private void GameScreen_Init()
        {
            this.gameWin = Window.GetWindow(this);
            this.gameWin.Background = Brushes.Black;
            this.gameWin.KeyDown += this.Screen_KeyDown;
            this.gameWin.KeyDown += this.Screen_KeyDownShoot;
            this.gameWin.KeyUp += this.Screen_KeyUp;
        }

        /// <summary>
        /// To pause the game and shoot every kind of lasers and get shield.
        /// </summary>
        /// <param name="sender">Sender object.</param>
        /// <param name="e">The last name to join.</param>
        private void Screen_KeyDownShoot(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.P)
            {
                this.dt.Stop();
                this.logic.DtMiliSeconds.Stop();
                this.logic.DtSeconds.Stop();
                PauseWindow pause = new PauseWindow(this.vm);
                pause.ShowDialog();
                if (pause.Exit == true)
                {
                    this.logic.GameExit += this.Logic_GameExit;
                    this.dt.Stop();
                    this.gameWin.Close();
                }
                else
                {
                    this.dt.Start();
                    this.logic.DtMiliSeconds.Start();
                    this.logic.DtSeconds.Start();
                }
            }

            if (this.vm.Player2 != null)
            {
                if (this.player1Death == false && e.Key == Key.Space)
                {
                    this.logic.Shoot(false);
                }

                if (this.player2Death == false && e.Key == Key.Q)
                {
                    this.logic.Shoot(true);
                }
            }
            else
            {
                if (e.Key == Key.Space)
                {
                    this.logic.Shoot(false);
                }
            }

            if (e.Key == Key.RightShift && this.shieldP1 > 0 && this.logic.ShieldOnP1 == false && this.player1Death == false)
            {
                this.vm.Player1.Shield = 2;
                this.logic.Shield(false);
                this.shieldP1--;
            }

            if (this.vm.Player2 != null)
            {
                if (e.Key == Key.E && this.shieldP2 > 0 && this.logic.ShieldOnP2 == false && this.player2Death == false)
                {
                    this.vm.Player2.Shield = 2;
                    this.logic.Shield(true);
                    this.shieldP2--;
                }
            }

            if (e.Key == Key.RightCtrl && this.megaLaserP1 > 0 && this.player1Death == false)
            {
                this.logic.ShootBig(false);
                this.megaLaserP1--;
            }

            if (this.vm.Player2 != null && e.Key == Key.LeftCtrl && this.megaLaserP2 > 0 && this.player2Death == false)
            {
                this.logic.ShootBig(true);
                this.megaLaserP2--;
            }
        }

        /// <summary>
        /// Examine that the pressed key list's contents.
        /// </summary>
        /// <param name="sender">Sender object.</param>
        /// <param name="e">Event parameter.</param>
        private void Screen_KeyDown(object sender, KeyEventArgs e)
        {
            if (!this.pressedKeys.Contains(e.Key))
            {
                this.pressedKeys.Add(e.Key);
            }

            foreach (Key item in this.pressedKeys)
            {
                this.PressKey(item);
            }                                                 
        }

        /// <summary>
        /// Set the actions to the keys.
        /// </summary>
        /// <param name="e">The pressed key parameter.</param>
        private void PressKey(Key e)
        {
            if (this.vm.Player2 != null)
            {
                switch (e)
                {
                    case Key.Left:
                        this.logic.OnFly(-10, 0, false); 
                        break;
                    case Key.Right:
                        this.logic.OnFly(10, 0, false);
                        break;
                    case Key.Up:
                        this.logic.OnFly(0, -10, false); 
                        break;
                    case Key.Down:
                        this.logic.OnFly(0, 10, false);
                        break;

                    case Key.A:
                        this.logic.OnFly(-10, 0, true);
                        break;
                    case Key.D:
                        this.logic.OnFly(10, 0, true); 
                        break;
                    case Key.W:
                        this.logic.OnFly(0, -10, true); 
                        break;
                    case Key.S:
                        this.logic.OnFly(0, 10, true); 
                        break;
                }
            }
            else
            {
                switch (e)
                {
                    case Key.Left:
                        this.logic.OnFly(-10, 0, false);
                        break;
                    case Key.Right:
                        this.logic.OnFly(10, 0, false); 
                        break;
                    case Key.Up:
                        this.logic.OnFly(0, -10, false); 
                        break;
                    case Key.Down:
                        this.logic.OnFly(0, 10, false); 
                        break;
                }
            }                    
        }

        /// <summary>
        /// Clear the pressed key list if the player release the button.
        /// </summary>
        /// <param name="sender">Sender object.</param>
        /// <param name="e">Key parameter.</param>
        private void Screen_KeyUp(object sender, KeyEventArgs e)
        {
            this.pressedKeys.Remove(e.Key);
            this.UpKey(e.Key);
        }

        /// <summary>
        /// To "stop" the player.
        /// </summary>
        /// <param name="e">Released key.</param>
        private void UpKey(Key e)
        {
            if (this.vm.Player2 != null)
            {
                switch (e)
                {
                    case Key.Left:
                        this.logic.OnFly(0, 0, false); 
                        break;
                    case Key.Right:
                        this.logic.OnFly(0, 0, false); 
                        break;
                    case Key.Up:
                        this.logic.OnFly(0, 0, false);
                        break;
                    case Key.Down:
                        this.logic.OnFly(0, 0, false);
                        break;                

                    case Key.A:
                        this.logic.OnFly(0, 0, true); 
                        break;
                    case Key.D:
                        this.logic.OnFly(0, 0, true); 
                        break;
                    case Key.W:
                        this.logic.OnFly(0, 0, true); 
                        break;
                    case Key.S:
                        this.logic.OnFly(0, 0, true);
                        break;
                }
            }
            else
            {
                switch (e)
                {
                    case Key.Left:
                        this.logic.OnFly(0, 0, false); 
                        break;
                    case Key.Right:
                        this.logic.OnFly(0, 0, false); 
                        break;
                    case Key.Up:
                        this.logic.OnFly(0, 0, false); 
                        break;
                    case Key.Down:
                        this.logic.OnFly(0, 0, false);
                        break;
                }
            }
        }

        /// <summary>
        /// Set the screen.
        /// </summary>
        /// <param name="sender">Sender object.</param>
        /// <param name="e">Event parameter.</param>
        private void Screen_Loaded(object sender, RoutedEventArgs e)
        {
            this.GameScreen_Init();
            this.logic.CurrentWindow = this.gameWin;
            this.gameWin.KeyDown += this.Screen_KeyDown;
            this.dt.Start();
        }
    }
   }