﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Media;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace Kosztyu_adam_gg2hlc_beadando
{
    /// <summary>
    /// Interaction logic for SinglePlayer.xaml
    /// </summary>
    public partial class SinglePlayerWindow : Window
    {
        ViewModel VM;
        SoundPlayer sp = new SoundPlayer();

        public SinglePlayerWindow()
        {
            InitializeComponent();
            VM = ViewModel.Get();
            sp.SoundLocation = "Theme.wav";
        }

        private void OnBackClick(object sender, RoutedEventArgs e)
        {
            DialogResult = false;
        }

        private void OnStartClick(object sender, RoutedEventArgs e)
        {

            if (textBox.Text == "")
                VM.Player1.Name = "ismeretlen";
            else
                VM.Player1.Name = textBox.Text;

            VM.Multiplayer = false;
            GameWindow gw = new GameWindow();
            this.Hide();
            sp.Stop();
            gw.ShowDialog();
            VM.Clear(VM);           
            this.Close();
        }
    }
}
