﻿namespace Kosztyu_adam_gg2hlc_beadando
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Media;
    using System.Text;
    using System.Threading.Tasks;
    using System.Windows;
    using System.Windows.Media;
    using System.Windows.Media.Imaging;
    using System.Windows.Threading;

    /// <summary>
    /// GameLogic class.
    /// </summary>
    public class GameLogic
    {
        /// <summary>
        /// Generate static random number.
        /// </summary>
        private static Random rnd = new Random();

        /// <summary>
        /// Time counter to count the time.
        /// </summary>
        private int timeCounter = 0;

        /// <summary>
        /// Time counter but in a loop.It's for the boss fight.
        /// </summary>
        private int bossLoop = 0;

        /// <summary>
        /// Count the deaths if the game is multiplayer.
        /// </summary>
        private int deathCounter = 2;

        /// <summary>
        /// Boss lasers position on X-axis.
        /// </summary>
        private int laserPositionX = 0;

        /// <summary>
        /// Boss lasers position on Y-axis.
        /// </summary>
        private int laserPositionY = 520;

        /// <summary>
        /// For the game over window.
        /// </summary>
        private int gameOverCounter = 0;

        /// <summary>
        /// Sound player.
        /// </summary>
        private SoundPlayer soundPlayer = new SoundPlayer();

        /// <summary>
        /// Constructor of the Game Logic class.
        /// Initializes a new instance of the <see cref="GameLogic"/> class.
        /// </summary>
        public GameLogic()
        {
            this.soundPlayer.SoundLocation = @"Resources\LaserEffect.wav";  
            this.DtMiliSeconds = new DispatcherTimer() { Interval = TimeSpan.FromMilliseconds(30) };
            this.DtSeconds = new DispatcherTimer() { Interval = TimeSpan.FromSeconds(1) };
            this.DtMiliSeconds.Tick += this.DtMiliS_Tick;
            this.DtSeconds.Tick += this.DtSec_Tick;
            this.DtMiliSeconds.Start();
            this.DtSeconds.Start();
        }

        /// <summary>
        /// Game over event.
        /// </summary>
        public event EventHandler GameOver;

        /// <summary>
        /// Game win event.
        /// </summary>
        public event EventHandler GameWin;

        /// <summary>
        /// Game exit event.
        /// </summary>
        public event EventHandler GameExit;

        /// <summary>
        /// Gets or sets smallest timer property.
        /// </summary>
        /// <value>The smallest timer value.</value>
        public DispatcherTimer DtMiliSeconds { get; set; }

        /// <summary>
        /// Gets or sets timer property.
        /// </summary>
        /// <value>Timer value.</value>
        public DispatcherTimer DtSeconds { get; set; }

        /// <summary>
        /// Gets or sets current window property.
        /// </summary>
        /// <value>Current window property.</value>
        public Window CurrentWindow { get; set; }

        /// <summary>
        /// Gets or sets view model property.
        /// </summary>
        /// <value>View model property.</value>
        public ViewModel VM { get; set; }

        /// <summary>
        /// Gets or sets shield property.
        /// </summary>
        /// <value>Logic value about the shield.</value>
        public bool ShieldOnP1 { get; set; }

        /// <summary>
        /// Gets or sets shield property.
        /// </summary>
        /// <value>Logic value about the shield.</value>
        public bool ShieldOnP2 { get; set; }

        /// <summary>
        /// Gets or sets points property.
        /// </summary>
        /// <value>Player point or players point.</value>
        public int PlayerPoints { get; set; }
      
        /// <summary>
        /// To move the player.It's not allowed the player or players to leave the screen.
        /// </summary>
        /// <param name="speedX">Speed on X-axis.</param>
        /// <param name="speedY">Speed on Y-axis.</param>
        /// <param name="player2">Logic value.The value decide if the game is single or multiplayer.</param>
        public void OnFly(int speedX, int speedY, bool player2) 
        {
            if (player2 == false)
            {
                if (this.VM.Player1.Center.X + speedX >= 0 && this.VM.Player1.Center.X + speedX <= this.CurrentWindow.ActualWidth && this.VM.Player1.Center.Y + speedY >= 0 && this.VM.Player1.Center.Y + speedY <= this.CurrentWindow.ActualHeight - (Player.BODYHEIGHT / 2))
                {
                    this.VM.Player1.Fly(speedX, speedY); 
                }
            }
            else
            {
                if (this.VM.Player2.Center.X + speedX >= 0 && this.VM.Player2.Center.X + speedX <= this.CurrentWindow.ActualWidth && this.VM.Player2.Center.Y + speedY >= 0 && this.VM.Player2.Center.Y + speedY <= this.CurrentWindow.ActualHeight - (Player.BODYHEIGHT / 2))
                {
                    this.VM.Player2.Fly(speedX, speedY); 
                }
            }   
        }

        /// <summary>
        /// To Shoot lasers.
        /// </summary>
        /// <param name="player2">Single or Multiplayer.</param>
        public void Shoot(bool player2) 
        {   
            if (player2 == false)
            {
                this.VM.Laser.Add(new Laser((int)this.VM.Player1.Center.X - 41, (int)this.VM.Player1.Center.Y + 40));
                this.VM.Laser.Add(new Laser((int)this.VM.Player1.Center.X + 41, (int)this.VM.Player1.Center.Y + 40));
                this.soundPlayer.Play();
            }
            else
            {
                this.VM.Laser.Add(new Laser((int)this.VM.Player2.Center.X - 41, (int)this.VM.Player2.Center.Y + 40));
                this.VM.Laser.Add(new Laser((int)this.VM.Player2.Center.X + 41, (int)this.VM.Player2.Center.Y + 40));
                this.soundPlayer.Play();
            }
        }

        /// <summary>
        /// Shoot big lasers.
        /// </summary>
        /// <param name="player2">Single or multiplayer.</param>
        public void ShootBig(bool player2)
        {
            if (player2 == false)
            {
                this.VM.Biglaser.Add(new BigLaser((int)this.VM.Player1.Center.X, (int)this.VM.Player1.Center.Y));
                this.soundPlayer.Play();
            }
            else
            {
                this.VM.Biglaser.Add(new BigLaser((int)this.VM.Player2.Center.X, (int)this.VM.Player2.Center.Y));
                this.soundPlayer.Play();
            }
        }

        /// <summary>
        /// Activate the shield.
        /// </summary>
        /// <param name="player2">Single or multiplayer.</param>
        public void Shield(bool player2)
        {
            if (player2 == false)
            {
                this.VM.Player1.ComponentImage = new BitmapImage(new Uri(@"pack://application:,,,/Resources/spaceshipwshield.png"));
                this.ShieldOnP1 = true;
            }
            else
            {
                this.VM.Player2.ComponentImage = new BitmapImage(new Uri(@"pack://application:,,,/Resources/t70spaceshipwshield.png"));
                this.ShieldOnP2 = true;
            }
        }
    
        /// <summary>
        /// Create the stars and enemies. It generate depends on time counter value.
        /// </summary>
        /// <param name="sender">Sender object.</param>
        /// <param name="e">Event parameter.</param>
        private void DtSec_Tick(object sender, EventArgs e)
        {
            this.timeCounter++;
            if (this.VM.FinalBoss.Center.Y < 270)
            {
                for (int i = 1; i < 30; i++)
                {
                    this.VM.Stars.Add(new Stars(rnd.Next(0, (int)this.CurrentWindow.ActualWidth), rnd.Next(-300, 0)));
                }
            }
            
            if (this.timeCounter > 3 && this.timeCounter < 13)
            {
                this.VM.Asteroids.Add(new Asteroids(rnd.Next(0, (int)this.CurrentWindow.ActualWidth)));
                this.VM.Asteroids.Add(new Asteroids(rnd.Next(0, (int)this.CurrentWindow.ActualWidth)));               
            }

            if (this.timeCounter > 18 && this.timeCounter < 32)
            {
                this.VM.Asteroids.Add(new Asteroids(rnd.Next(0, (int)this.CurrentWindow.ActualWidth)));
                this.VM.Asteroids.Add(new Asteroids(rnd.Next(0, (int)this.CurrentWindow.ActualWidth)));
                this.VM.Asteroids.Add(new Asteroids(rnd.Next(0, (int)this.CurrentWindow.ActualWidth)));
            }

            if (this.timeCounter > 36 && this.timeCounter < 47)
            {
                int position = rnd.Next(0, (int)this.CurrentWindow.ActualWidth - EnemySpaceship.ESPACESHIPWIDTH);
                this.VM.Enemyships.Add(new EnemySpaceship(position));
                this.VM.Enemylaser.Add(new EnemyLaser(position));
            }

            if (this.timeCounter > 53 && this.timeCounter < 71)
            {
                this.VM.Asteroids.Add(new Asteroids(rnd.Next(-400, (int)this.CurrentWindow.ActualWidth)));
                this.VM.Asteroids.Add(new Asteroids(rnd.Next(-400, (int)this.CurrentWindow.ActualWidth)));
                this.VM.Asteroids.Add(new Asteroids(rnd.Next(-400, (int)this.CurrentWindow.ActualWidth)));
                this.VM.Asteroids.Add(new Asteroids(rnd.Next(-400, (int)this.CurrentWindow.ActualWidth)));
            }

            if (this.timeCounter > 75 && this.timeCounter < 87)
            {
                int position1 = rnd.Next(0, (int)this.CurrentWindow.ActualWidth - EnemySpaceship.ESPACESHIPWIDTH);
                this.VM.Enemyships.Add(new EnemySpaceship(position1));
                this.VM.Enemylaser.Add(new EnemyLaser(position1));
                int position2 = rnd.Next(0, (int)this.CurrentWindow.ActualWidth - EnemySpaceship.ESPACESHIPWIDTH);
                this.VM.Enemyships.Add(new EnemySpaceship(position2));
                this.VM.Enemylaser.Add(new EnemyLaser(position2));
            }
            
            if (this.VM.FinalBoss.Center.Y >= 270)
            {
                this.bossLoop++;
                if (this.bossLoop > 2 && this.bossLoop < 7)
                {
                    this.VM.Bosslaser.Add(new BossLaser(this.laserPositionX, 20));
                    this.VM.Bosslaser.Add(new BossLaser(this.laserPositionY, 20));
                    this.laserPositionX = this.laserPositionX + 50;
                    this.laserPositionY = this.laserPositionY - 50;
                }

                if (this.bossLoop > 9 && this.bossLoop < 20)
                {
                    if (this.bossLoop % 2 == 0)
                    {
                        int[] positions = { 30, 140, 250, 360, 470 };
                        int hole = rnd.Next(0, 5);
                        for (int i = 0; i < 5; i++)
                        {
                            if (i != hole)
                            {
                                this.VM.Bossenergyball.Add(new BossEnergyBall(positions[i], 180));
                            }
                        }
                    }
                }

                if (this.bossLoop > 22 && this.bossLoop < 27)
                {
                    this.VM.Bosslaser.Add(new BossLaser(60, 20));
                    this.VM.Bosslaser.Add(new BossLaser(140, 20));
                    this.VM.Bosslaser.Add(new BossLaser(380, 20));
                    this.VM.Bosslaser.Add(new BossLaser(460, 20));
                }

                if (this.bossLoop > 29 && this.bossLoop < 31)
                {
                    this.VM.Bossenergyball.Add(new BossEnergyBall(0, 180));
                    this.VM.Bossenergyball.Add(new BossEnergyBall(100, 80));
                    this.VM.Bossenergyball.Add(new BossEnergyBall(200, -20));
                    this.VM.Bossenergyball.Add(new BossEnergyBall(100, -120));
                    this.VM.Bossenergyball.Add(new BossEnergyBall(0, -220));

                    this.VM.Bossenergyball.Add(new BossEnergyBall(400, 180));
                    this.VM.Bossenergyball.Add(new BossEnergyBall(500, 80));
                    this.VM.Bossenergyball.Add(new BossEnergyBall(600, -20));
                    this.VM.Bossenergyball.Add(new BossEnergyBall(500, -120));
                    this.VM.Bossenergyball.Add(new BossEnergyBall(400, -220));
                    this.VM.Bossenergyball.Add(new BossEnergyBall(300, -320));
                    this.VM.Bossenergyball.Add(new BossEnergyBall(200, -420));
                }

                if (this.bossLoop >= 35)
                {
                    this.bossLoop = 0;
                    this.laserPositionX = 0;
                    this.laserPositionY = 520;
                }
            }
        }

        /// <summary>
        /// Watch the movement and collision on every component.
        /// </summary>
        /// <param name="sender">Sender object.</param>
        /// <param name="e">Event parameter.</param>
        private void DtMiliS_Tick(object sender, EventArgs e)
        {
            if (this.GameExit != null)
            {
                this.DtMiliSeconds.Stop();
                this.DtSeconds.Stop();
                this.GameExit(this, EventArgs.Empty);          
            }

            List<Component> temp = new List<Component>();

            foreach (Laser item in this.VM.Laser)
            {
                if (item.Shape.Bounds.Bottom <= 0)
                {
                    temp.Add(item);
                }
                else
                {
                    item.Shoot();
                }
            }

            foreach (BigLaser item in this.VM.Biglaser)
            {
                if (item.Shape.Bounds.Bottom <= 0)
                {
                    temp.Add(item);
                }
                else
                {
                    item.Shoot();
                }
            }

            foreach (Stars item in this.VM.Stars)
            {
                if (this.VM.FinalBoss.Center.Y < 270)
                {
                    if (item.Shape.Bounds.Bottom >= this.CurrentWindow.ActualHeight)
                    {
                        temp.Add(item);
                    }
                    else if (this.VM.FinalBoss.Center.Y < 50)
                    {
                        item.Fly(11);
                    }
                    else if (this.VM.FinalBoss.Center.Y >= 50 && this.VM.FinalBoss.Center.Y < 105)
                    {
                        item.Fly(9);
                    }
                    else if (this.VM.FinalBoss.Center.Y >= 105 && this.VM.FinalBoss.Center.Y < 160)
                    {
                        item.Fly(7);
                    }
                    else if (this.VM.FinalBoss.Center.Y >= 160 && this.VM.FinalBoss.Center.Y < 215)
                    {
                        item.Fly(5);
                    }
                    else if (this.VM.FinalBoss.Center.Y >= 215 && this.VM.FinalBoss.Center.Y < 270)
                    {
                        item.Fly(3);
                    }
                    else if (this.VM.FinalBoss.Center.Y == 265)
                    {
                        item.Fly(0);
                    }
                }
            }

            foreach (BossLaser item in this.VM.Bosslaser) 
            {
                if (item.Shape.Bounds.Top > this.CurrentWindow.ActualHeight)
                {
                    temp.Add(item);
                }
                else if (this.VM.Player2 != null)
                {
                    if (item.Collision(this.VM.Player1))
                    {
                        if (this.ShieldOnP1 == true)
                        {
                            temp.Add(item);
                            this.VM.Player1.Shield -= 2;
                            if (this.VM.Player1.Shield <= 0)
                            {
                                this.VM.Player1.ComponentImage = new BitmapImage(new Uri(@"pack://application:,,,/Resources/spaceship.png"));
                                this.ShieldOnP1 = false;
                            }
                        }
                        else
                        {
                            this.VM.Player1.Life -= 1;
                            if (this.VM.Player1.Life == 0)
                            {
                                this.deathCounter--;
                                this.gameOverCounter++;
                                if (this.GameOver != null && this.deathCounter == 0 && this.gameOverCounter == 2)
                                {
                                    this.DtMiliSeconds.Stop();
                                    this.DtSeconds.Stop();
                                    this.GameOver(this, EventArgs.Empty);
                                }
                            }
                        }
                    }
                    else if (item.Collision(this.VM.Player2))
                    {
                        if (this.ShieldOnP2 == true)
                        {
                            temp.Add(item);
                            this.VM.Player2.Shield -= 2;
                            if (this.VM.Player2.Shield <= 0)
                            {
                                this.VM.Player2.ComponentImage = new BitmapImage(new Uri(@"pack://application:,,,/Resources/t70x_spaceship.png"));
                                this.ShieldOnP2 = false;
                            }
                        }
                        else
                        {
                            this.VM.Player2.Life -= 1;
                            if (this.VM.Player2.Life == 0)
                            {
                                this.deathCounter--;
                                this.gameOverCounter++;
                                if (this.GameOver != null && this.deathCounter == 0 && this.gameOverCounter == 2)
                                {
                                    this.DtMiliSeconds.Stop();
                                    this.DtSeconds.Stop();
                                    this.GameOver(this, EventArgs.Empty);
                                }
                            }
                        }
                    }
                    else
                    {
                        item.Fly();
                    }
                }
                else
                {
                    if (item.Collision(this.VM.Player1))
                    {
                        if (this.ShieldOnP1 == true)
                        {
                            temp.Add(item);
                            this.VM.Player1.Shield -= 2;
                            if (this.VM.Player1.Shield <= 0)
                            {
                                this.VM.Player1.ComponentImage = new BitmapImage(new Uri(@"pack://application:,,,/Resources/spaceship.png"));
                                this.ShieldOnP1 = false;
                            }
                        }
                        else
                        {
                            this.VM.Player1.Life -= 1;
                            if (this.VM.Player1.Life == 0)
                            {
                                this.gameOverCounter++;
                                if (this.GameOver != null && this.gameOverCounter == 1)
                                {
                                    this.DtMiliSeconds.Stop();
                                    this.DtSeconds.Stop();
                                    this.GameOver(this, EventArgs.Empty);
                                }
                            }
                        }
                    }
                    else
                    {
                        item.Fly();
                    }
                }
            }

            foreach (BossEnergyBall item in this.VM.Bossenergyball)
            {
                if (item.Shape.Bounds.Top >= this.CurrentWindow.ActualHeight)
                {
                    temp.Add(item);
                }
                else if (this.VM.Player2 != null)
                {
                    if (item.Collision(this.VM.Player1))
                    {
                        if (this.ShieldOnP1 == true)
                        {
                            temp.Add(item);
                            this.VM.Player1.Shield -= 2;
                            if (this.VM.Player1.Shield <= 0)
                            {
                                this.VM.Player1.ComponentImage = new BitmapImage(new Uri(@"pack://application:,,,/Resources/spaceship.png"));
                                this.ShieldOnP1 = false;
                            }
                        }
                        else
                        {
                            temp.Add(item);
                            this.VM.Player1.Life -= 1;
                            if (this.VM.Player1.Life == 0)
                            {
                                this.deathCounter--;
                                this.gameOverCounter++;
                                if (this.GameOver != null && this.deathCounter == 0 && this.gameOverCounter == 2)
                                {
                                    this.DtMiliSeconds.Stop();
                                    this.DtSeconds.Stop();
                                    this.GameOver(this, EventArgs.Empty);
                                }
                            }
                        }
                    }
                    else if (item.Collision(this.VM.Player2))
                    {
                        if (this.ShieldOnP2 == true)
                        {
                            temp.Add(item);
                            this.VM.Player2.Shield -= 2;
                            if (this.VM.Player2.Shield <= 0)
                            {
                                this.VM.Player2.ComponentImage = new BitmapImage(new Uri(@"pack://application:,,,/Resources/t70x_spaceship.png"));
                                this.ShieldOnP2 = false;
                            }
                        }
                        else
                        {
                            temp.Add(item);
                            this.VM.Player2.Life -= 1;
                            if (this.VM.Player2.Life == 0)
                            {
                                this.deathCounter--;
                                this.gameOverCounter++;
                                if (this.GameOver != null && this.deathCounter == 0 && this.gameOverCounter == 2)
                                {
                                    this.DtMiliSeconds.Stop();
                                    this.DtSeconds.Stop();
                                    this.GameOver(this, EventArgs.Empty);
                                }
                            }
                        }
                    }
                    else
                    {
                        item.Fly();
                    }
                }
                else
                {
                    if (item.Collision(this.VM.Player1))
                    {
                        if (this.ShieldOnP1 == true)
                        {
                            temp.Add(item);
                            this.VM.Player1.Shield -= 2;
                            if (this.VM.Player1.Shield <= 0)
                            {
                                this.VM.Player1.ComponentImage = new BitmapImage(new Uri(@"pack://application:,,,/Resources/spaceship.png"));
                                this.ShieldOnP1 = false;
                            }
                        }
                        else
                        {
                            temp.Add(item);
                            this.VM.Player1.Life -= 1;
                            if (this.VM.Player1.Life == 0)
                            {
                                this.gameOverCounter++;
                                if (this.GameOver != null && this.gameOverCounter == 1)
                                {
                                    this.DtMiliSeconds.Stop();
                                    this.DtSeconds.Stop();
                                    this.GameOver(this, EventArgs.Empty);
                                }
                            }
                        }
                    }
                    else
                    {
                        item.Fly();
                    }
                }
            }

            foreach (Asteroids item in this.VM.Asteroids)
            {
                this.ComponentsLogic(item, temp);

                if (this.timeCounter < 50)
                {
                    item.FlyVertical(3);
                }
                else if (this.timeCounter > 52)
                {
                    item.FlyCross(4, 12);
                }
            }

            foreach (EnemySpaceship item in this.VM.Enemyships)
            {
                this.ComponentsLogic(item, temp);
                item.Fly();
            }

            foreach (EnemyLaser item in this.VM.Enemylaser)
            {
                this.ComponentsLogic(item, temp);
                item.Fly();
            }

            if (this.timeCounter > 91) 
            {
                this.BossEvents();
            }

            foreach (Component item in this.VM.Laser)
            {
                foreach (Asteroids item2 in this.VM.Asteroids)
                {                 
                    if (item.Collision(item2))
                    {
                        temp.Add(item2);                                      
                        temp.Add(item);
                        this.PlayerPoints += 10;
                    }
                }

                foreach (EnemySpaceship item2 in this.VM.Enemyships)
                {                   
                    if (item.Collision(item2))
                    {
                        item2.Life -= 1;
                        temp.Add(item);
                        if (item2.Life <= 0)
                        {
                            temp.Add(item);
                            temp.Add(item2);
                            this.PlayerPoints += 30;
                        }
                    }
                }

                foreach (BossEnergyBall item2 in this.VM.Bossenergyball)
                {
                    if (item.Collision(item2))
                    {
                        temp.Add(item);
                    }
                }

                foreach (BossLaser item2 in this.VM.Bosslaser)
                {
                    if (item.Collision(item2))
                    {
                        temp.Add(item);
                    }
                }

                if (item.Collision(this.VM.FinalBoss))
                {
                    temp.Add(item);
                    this.VM.FinalBoss.Life -= 50;
                }            
            }

            foreach (Component item in this.VM.Biglaser)
            {
                foreach (Asteroids item2 in this.VM.Asteroids)
                {
                    if (item.Collision(item2))
                    {
                        temp.Add(item2);
                        this.PlayerPoints += 15;
                    }
                }

                foreach (EnemySpaceship item2 in this.VM.Enemyships)
                {
                    if (item.Collision(item2))
                    {
                        temp.Add(item2);
                        this.PlayerPoints += 15;
                    }
                }

                foreach (EnemyLaser item2 in this.VM.Enemylaser)
                {
                    if (item.Collision(item2))
                    {
                        temp.Add(item2);
                        this.PlayerPoints += 5;
                    }
                }

                foreach (BossEnergyBall item2 in this.VM.Bossenergyball)
                {
                    if (item.Collision(item2))
                    {
                        temp.Add(item);
                    }
                }

                foreach (BossLaser item2 in this.VM.Bosslaser)
                {
                    if (item.Collision(item2))
                    {
                        temp.Add(item);
                    }
                }

                if (item.Collision(this.VM.FinalBoss))
                {
                    this.VM.FinalBoss.Life -= 500;
                    temp.Add(item);
                }
            }

            foreach (Component item in temp)
            {
                if (item is Laser)
                {
                    this.VM.Laser.Remove(item as Laser);
                }

                if (item is BigLaser)
                {
                    this.VM.Biglaser.Remove(item as BigLaser);
                }

                if (item is Asteroids)
                {
                    this.VM.Asteroids.Remove(item as Asteroids);
                }

                if (item is EnemySpaceship)
                {
                    this.VM.Enemyships.Remove(item as EnemySpaceship);
                }

                if (item is EnemyLaser)
                {
                    this.VM.Enemylaser.Remove(item as EnemyLaser);
                }

                if (item is Stars)
                {
                    this.VM.Stars.Remove(item as Stars);
                }

                if (item is BossEnergyBall)
                {
                    this.VM.Bossenergyball.Remove(item as BossEnergyBall);
                }

                if (item is BossLaser)
                {
                    this.VM.Bosslaser.Remove(item as BossLaser);
                }
            }
        }

        /// <summary>
        /// Most of the components have the same logic except boss energy ball and boss laser.
        /// </summary>
        /// <param name="comp">Examine the collision of the component.</param>
        /// <param name="compList">Temporary list.</param>
        private void ComponentsLogic(Component comp, List<Component> compList)
        {
            if (comp.Shape.Bounds.Top >= this.CurrentWindow.ActualHeight)
            {
                compList.Add(comp);
            }
            else if (this.VM.Player2 != null)
            {
                if (comp.Collision(this.VM.Player1) && !comp.Impact)
                {
                    if (this.ShieldOnP1 == true)
                    {
                        compList.Add(comp);
                        this.VM.Player1.Shield -= 1;
                        if (this.VM.Player1.Shield <= 0)
                        {
                            this.VM.Player1.ComponentImage = new BitmapImage(new Uri(@"pack://application:,,,/Resources/spaceship.png"));
                            this.ShieldOnP1 = false;
                        }
                    }
                    else
                    {
                        this.VM.Player1.Life -= 1;
                        comp.Impact = true;
                        compList.Add(comp);
                        if (this.VM.Player1.Life == 0)
                        {
                            this.deathCounter--;
                            this.gameOverCounter++;
                            if (this.GameOver != null && this.deathCounter == 0 && this.gameOverCounter == 2)
                            {
                                this.DtMiliSeconds.Stop();
                                this.DtSeconds.Stop();
                                this.GameOver(this, EventArgs.Empty);
                            }
                        }
                    }
                }
                else if (comp.Collision(this.VM.Player2) && !comp.Impact)
                {
                    if (this.ShieldOnP2 == true)
                    {
                        compList.Add(comp);
                        this.VM.Player2.Shield -= 1;
                        if (this.VM.Player2.Shield <= 0)
                        {
                            this.VM.Player2.ComponentImage = new BitmapImage(new Uri(@"pack://application:,,,/Resources/t70x_spaceship.png"));
                            this.ShieldOnP2 = false;
                        }
                    }
                    else
                    {
                        this.VM.Player2.Life -= 1;
                        comp.Impact = true;
                        compList.Add(comp);
                        if (this.VM.Player2.Life == 0)
                        {
                            this.deathCounter--;
                            this.gameOverCounter++;
                            if (this.GameOver != null && this.deathCounter == 0 && this.gameOverCounter == 2)
                            {
                                this.DtMiliSeconds.Stop();
                                this.DtSeconds.Stop();
                                this.GameOver(this, EventArgs.Empty);
                            }
                        }
                    }
                }
            }
            else
            {
                if (comp.Collision(this.VM.Player1) && !comp.Impact)
                {
                    if (this.ShieldOnP1 == true)
                    {
                        compList.Add(comp);
                        this.VM.Player1.Shield -= 1;
                        if (this.VM.Player1.Shield <= 0)
                        {
                            this.VM.Player1.ComponentImage = new BitmapImage(new Uri(@"pack://application:,,,/Resources/spaceship.png"));
                            this.ShieldOnP1 = false;
                        }
                    }
                    else
                    {
                        this.VM.Player1.Life -= 1;
                        comp.Impact = true;
                        compList.Add(comp);
                        if (this.VM.Player1.Life == 0)
                        {
                            this.gameOverCounter++;
                            if (this.GameOver != null && this.gameOverCounter == 1)
                            {
                                this.DtMiliSeconds.Stop();
                                this.DtSeconds.Stop();
                                this.GameOver(this, EventArgs.Empty);
                            }
                        }
                    }
                }
            }  
        }

        /// <summary>
        /// To move the final boss and watch the players death.
        /// </summary>
        private void BossEvents()
        {
            if (this.VM.FinalBoss.Center.Y < 270)
            {
                this.VM.FinalBoss.Fly();
            }

            if (this.VM.Player1.Collision(this.VM.FinalBoss))
            {
                this.VM.Player1.Life -= 1;
            }

            if (this.VM.Player2 != null)
            {
                if (this.deathCounter <= 0)
                {
                    this.gameOverCounter++;
                    if (this.GameOver != null && this.gameOverCounter == 2)
                    {
                        this.DtMiliSeconds.Stop();
                        this.DtSeconds.Stop();
                        this.GameOver(this, EventArgs.Empty);
                    }
                }
            }
            else
            {
                if (this.VM.Player1.Life <= 0)
                {
                    this.gameOverCounter++;
                    if (this.GameOver != null && this.gameOverCounter == 1)  
                    {
                        this.DtMiliSeconds.Stop();
                        this.DtSeconds.Stop();
                        this.GameOver(this, EventArgs.Empty);
                    }
                }
            }

            if (this.VM.FinalBoss.Life <= 0)
            {
                this.PlayerPoints += 500;
                if (this.GameWin != null)
                {
                    this.DtMiliSeconds.Stop();
                    this.DtSeconds.Stop();
                    this.GameWin(this, EventArgs.Empty);
                }
            }           
        }     
    }
}
