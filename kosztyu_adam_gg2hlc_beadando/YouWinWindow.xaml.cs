﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.IO;

namespace Kosztyu_adam_gg2hlc_beadando
{
    /// <summary>
    /// Interaction logic for YouWinWindow.xaml
    /// </summary>
    public partial class YouWinWindow : Window
    {
        public YouWinWindow(int Score, int Life, string Name)
        {
            InitializeComponent();
            Init(Score, Life);
            SaveScore(Name, RealPoints(Score,Life));
        }
        private void Init(int score,int life)
        {
            scoreLabel.Content = "Pontszámod: " + score;
            lifeLabel.Content = "Bónusz: Élet(" + life+") x 200";
            realscoreLabel.Content = "Pontszámod: "+ (score + (life * 200));
        }
        private int RealPoints(int score, int life)
        {
            return score + (life * 200);
        }

        private void OnMenuClick(object sender, RoutedEventArgs e)
        {
            DialogResult = true;
        }

        private void SaveScore(string name, int score)
        {
            string[] Lines = new string[12];
            int lineCounter = 0;
            string temp = "";

            StreamWriter SW = new StreamWriter("Scores.txt", true);
            {
                SW.WriteLine(name + ":" + score);
            }
            SW.Close();

            StreamReader SR = new StreamReader("Scores.txt");
            do
            {
                Lines[lineCounter] = SR.ReadLine();
                lineCounter++;

            } while (!SR.EndOfStream);

            for (int i = 0; i < lineCounter; i++) 
            {
                string[] oneLine = Lines[i].Split(':'); 
                string playerName = oneLine[0]; 
                int playerScore = int.Parse(oneLine[1]);     

                for (int k = i; k < lineCounter; k++)
                {
                    string[] nextLine = Lines[k].Split(':');
                    int nextScore = int.Parse(nextLine[1]);

                    if (nextScore > playerScore)
                    {
                        temp = Lines[i];
                        Lines[i] = Lines[k];
                        Lines[k] = temp;
                    }
                }
            }
            SR.Close();
            StreamWriter SW2 = new StreamWriter("Scores.txt", false);
            if (lineCounter < 10)
            {
                for (int i = 0; i < lineCounter; i++)
                {
                    SW2.WriteLine(Lines[i]);
                }
            }

            else 
            {
                for (int i = 0; i < 10; i++)
                {
                    SW2.WriteLine(Lines[i]);
                }
            }
            SW2.Close();
        }                   
    }
}
