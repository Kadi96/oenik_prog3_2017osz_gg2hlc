﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Media;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Kosztyu_adam_gg2hlc_beadando
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public SoundPlayer soundPlayer = new SoundPlayer();

        ViewModel VM;
        public MainWindow()
        {           
            InitializeComponent();

            soundPlayer.SoundLocation = @"Resources\Theme.wav";
            VM = ViewModel.Get();
        }

        #region CheckBoxSound
        private void CheckBoxChecked(object sender, RoutedEventArgs e)
        {
                soundPlayer.Play();         
        }

        private void CheckBoxUnchecked(object sender, RoutedEventArgs e)
        {
            soundPlayer.Stop();
        }
        #endregion

        private void SinglePlayerClick(object sender, RoutedEventArgs e)
        {
            SinglePlayerWindow spWindow = new SinglePlayerWindow();
            this.Hide();
            spWindow.ShowDialog();
            this.Show();
        }

        private void MultiPlayerClick(object sender, RoutedEventArgs e)
        {
            MultiPlayerWindow mpWindow = new MultiPlayerWindow();
            this.Hide();
            mpWindow.ShowDialog();
            this.Show();
        }

        private void TopListClick(object sender, RoutedEventArgs e)
        {
            TopListWindow tlWindow = new TopListWindow();
            this.Hide();
            tlWindow.ShowDialog();
            this.Show();
        }


        private void MadeByClick(object sender, RoutedEventArgs e)
        {
            MadeByWindow mbWindow = new MadeByWindow();
            this.Hide();
            mbWindow.ShowDialog();
            this.Show();
        }

        private void OnInformationsClick(object sender, RoutedEventArgs e)
        {
            InformationsWindow infoWin = new InformationsWindow();
            this.Hide();
            infoWin.ShowDialog();
            this.Show();
        }
       
        private void ExitClick(object sender, RoutedEventArgs e)
        {
            Application.Current.Shutdown();
        }
    }
}
