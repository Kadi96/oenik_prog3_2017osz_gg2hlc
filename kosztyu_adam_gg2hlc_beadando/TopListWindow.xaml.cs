﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace Kosztyu_adam_gg2hlc_beadando
{
    /// <summary>
    /// Interaction logic for TopListWindow.xaml
    /// </summary>
    public partial class TopListWindow : Window
    {
        int lineCounter = 0;
        public TopListWindow()
        {
            InitializeComponent();
            ScoreReader();
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            DialogResult = false;
        }

        private void ScoreReader()
        {
            if (!File.Exists("Scores.txt"))
            {
                name1.Content = ""; score1.Content = "";
                name2.Content = ""; score2.Content = "";
                name3.Content = ""; score3.Content = "";
                name4.Content = ""; score4.Content = "";
                name5.Content = ""; score5.Content = "";
                name6.Content = ""; score6.Content = "";
                name7.Content = ""; score7.Content = "";
                name8.Content = ""; score8.Content = "";
                name9.Content = ""; score9.Content = "";
                name10.Content = ""; score10.Content = "";
            }
            else
            {
                StreamReader SR = new StreamReader("Scores.txt");
                do
                {
                    string[] line = SR.ReadLine().Split(':');
                    string name = line[0];
                    int score = int.Parse(line[1]);
                    lineCounter++;
                    switch (lineCounter)
                    {
                        case 1: name1.Content = name; score1.Content = score; break;
                        case 2: name2.Content = name; score2.Content = score; break;
                        case 3: name3.Content = name; score3.Content = score; break;
                        case 4: name4.Content = name; score4.Content = score; break;
                        case 5: name5.Content = name; score5.Content = score; break;
                        case 6: name6.Content = name; score6.Content = score; break;
                        case 7: name7.Content = name; score7.Content = score; break;
                        case 8: name8.Content = name; score8.Content = score; break;
                        case 9: name9.Content = name; score9.Content = score; break;
                        case 10: name10.Content = name; score10.Content = score; break;
                        default: break;
                    }
                } while (!SR.EndOfStream);
            }
        }
    }
}
