﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Media;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.Windows.Threading;

namespace Kosztyu_adam_gg2hlc_beadando
{
    /// <summary>
    /// Interaction logic for OpenWindow.xaml
    /// </summary>
    public partial class OpenWindow : Window
    {
        private const double FONTDIF = 0.15;

        private DispatcherTimer dt;
        private Canvas canvas;
        private int timeCounter = 0; 
        private int setLogoLeft = -180;
        private int setLogoTop = 0;
        private int moveStackPanel = 800;
        ImageBrush imgBrush = new ImageBrush();
        private SoundPlayer soundPlayer = new SoundPlayer();

        public OpenWindow()
        {
            InitializeComponent();
            canvas = (Canvas)this.Content;
            dt = new DispatcherTimer() { Interval = TimeSpan.FromMilliseconds(30) };
            dt.Tick += timer_Tick;
            dt.Start();
            imgBrush.ImageSource = new BitmapImage(new Uri(@"pack://application:,,,/Resources/menu_background.png"));
            canvas.Background = Brushes.Black;
            logo.Visibility = Visibility.Hidden;
        }

        void timer_Tick(object sender, EventArgs e)
        {
            timeCounter++; 
            if (timeCounter % 2 == 0 && timeCounter >= 100)
                img.Opacity = img.Opacity - 0.1;

            if (timeCounter == 130) 
            {
                Canvas.SetTop(logo, 0);
                logo.Visibility = Visibility.Visible;
                canvas.Background = imgBrush;
                soundPlayer.SoundLocation = @"Resources\Theme.wav";
                //soundPlayer.Play();
            }

            if (timeCounter % 5 == 0 && timeCounter > 130 && logo.Height > 0)
            {
                setLogoLeft = setLogoLeft + 5;
                setLogoTop = setLogoTop + 5;
                logo.Height = logo.Height - 10;
                logo.Width = logo.Width - 10;
                Canvas.SetLeft(logo, setLogoLeft);
                Canvas.SetTop(logo, setLogoTop);
            }

            if (timeCounter > 300)
                Canvas.SetTop(panel, moveStackPanel--);

            if (timeCounter > 320 && timeCounter % 5 == 0 && firsLine.FontSize > 1) 
            {
                firsLine.FontSize = firsLine.FontSize - FONTDIF;
                if (timeCounter > 340 && timeCounter % 5 == 0 && secondLine.FontSize > 1) 
                    secondLine.FontSize = secondLine.FontSize - FONTDIF;

                if (timeCounter > 370 && timeCounter % 5 == 0 && thirdLine.FontSize > 1)
                    thirdLine.FontSize = thirdLine.FontSize - FONTDIF;

                if (timeCounter > 400 && timeCounter % 5 == 0 && fourthLine.FontSize > 1)
                    fourthLine.FontSize = fourthLine.FontSize - FONTDIF;

                if (timeCounter > 430 && timeCounter % 5 == 0 && fifthLine.FontSize > 1)
                    fifthLine.FontSize = fifthLine.FontSize - FONTDIF;

                if (timeCounter > 460 && timeCounter % 5 == 0 && sixthLine.FontSize > 1)
                    sixthLine.FontSize = sixthLine.FontSize - FONTDIF;

                if (timeCounter > 490 && timeCounter % 5 == 0 && seventhLine.FontSize > 1)
                    seventhLine.FontSize = seventhLine.FontSize - FONTDIF;

                if (timeCounter > 520 && timeCounter % 5 == 0 && eightLine.FontSize > 1)
                    eightLine.FontSize = eightLine.FontSize - FONTDIF;

                if (timeCounter > 550 && timeCounter % 5 == 0 && ninthLine.FontSize > 1)
                    ninthLine.FontSize = ninthLine.FontSize - FONTDIF;

                if (timeCounter > 580 && timeCounter % 5 == 0 && tenthLine.FontSize > 1)
                    tenthLine.FontSize = tenthLine.FontSize - FONTDIF;
            }
        }

        private void OnSkipClick(object sender, RoutedEventArgs e)
        {
            dt.Stop();
            MainWindow mainWindow = new MainWindow();
            this.Close();
            mainWindow.ShowDialog();
        }
    }
}
