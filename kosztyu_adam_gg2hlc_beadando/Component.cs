﻿namespace Kosztyu_adam_gg2hlc_beadando
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using System.Windows;
    using System.Windows.Media;
    using System.Windows.Media.Imaging;

    /// <summary>
    /// The base component of every object.
    /// </summary>
    public abstract class Component
    {
        /// <summary>
        /// Image of the component.
        /// </summary>
        private BitmapImage componentImage;

        /// <summary>
        /// Shape of the component.
        /// </summary>
        private Geometry shape;

        /// <summary>
        /// X coordinate.
        /// </summary>
        private double x;

        /// <summary>
        /// Y coordinate.
        /// </summary>
        private double y;

        /// <summary>
        /// Impact logic.
        /// </summary>
        private bool impact;

        /// <summary>
        /// Gets or sets X coordinate.
        /// </summary>
        /// <value>The name of the customer.</value>
        public double X
        {
            get { return this.x; }
            set { this.x = value; }
        }

        /// <summary>
        /// Gets or sets Y coordinate.
        /// </summary>
        /// <value>The name of the customer.</value>
        public double Y
        {
            get { return this.y; }
            set { this.y = value; }
        }

        /// <summary>
        /// Gets or sets impact property.
        /// </summary>
        /// <value>The name of the customer.</value>
        public bool Impact
        {
            get { return this.impact; }
            set { this.impact = value; }
        }

        /// <summary>
        /// Gets or sets the component image.
        /// </summary>
        /// <value>The name of the customer.</value>
        public BitmapImage ComponentImage
        {
            get { return this.componentImage; }
            set { this.componentImage = value; }
        }

        /// <summary>
        /// Gets the center point.
        /// </summary>
        /// <value>The name of the customer.</value>
        public Point Center
        {
            get { return new Point(this.x, this.y); }
        }

        /// <summary>
        /// Gets or sets the component's shape.
        /// </summary>
        /// <value>The name of the customer.</value>
        public Geometry Shape
        {
            get
            {
                this.shape.Transform = new TranslateTransform(this.x, this.y);
                return this.shape;
            }
            set
            {
                this.shape = value;
            }
        }

        /// <summary>
        /// Watch the collision between two components.
        /// </summary>
        /// <param name="otherComp">Other Component.</param>
        /// <returns>Returns true or false.</returns>
        public bool Collision(Component otherComp)
        {
            PathGeometry intersection = Geometry.Combine(this.Shape, otherComp.Shape, GeometryCombineMode.Intersect, null);
            return intersection.GetArea() > 0;
        }
    }
}
