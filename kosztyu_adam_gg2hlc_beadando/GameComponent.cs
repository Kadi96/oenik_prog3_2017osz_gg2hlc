﻿namespace Kosztyu_adam_gg2hlc_beadando
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using System.Windows;
    using System.Windows.Controls;
    using System.Windows.Media;
    using System.Windows.Media.Imaging;
    using System.Windows.Shapes;

    /// <summary>
    /// Player class.
    /// </summary>
    public class Player : Component 
    {
        /// <summary>
        /// Player's width.
        /// </summary>
        public const int BODYWIDTH = 90;

        /// <summary>
        /// Player's height.
        /// </summary>
        public const int BODYHEIGHT = 102;

        /// <summary>
        /// Player's name.
        /// </summary>
        private string name;

        /// <summary>
        /// Heart image.
        /// </summary>
        private BitmapImage lifeimage;

        /// <summary>
        /// Number of the shields.
        /// </summary>
        private int shield;

        /// <summary>
        /// Player's life.
        /// </summary>
        private int life;

        /// <summary>
        /// Constructor of the player.
        /// Initializes a new instance of the <see cref="Player"/> class.
        /// </summary>
        /// <param name="secondPlayer">Logic value. If it is true, then the game is on multiplayer.</param>
        public Player(bool secondPlayer)
        {
            this.name = this.Name;
            this.X = 300;
            this.Y = 600;
            this.Life = 3;
            if (secondPlayer)
            {
                this.ComponentImage = new BitmapImage(new Uri(@"pack://application:,,,/Resources/spaceship.png"));
            }
            else
            {
                this.ComponentImage = new BitmapImage(new Uri(@"pack://application:,,,/Resources/t70x_spaceship.png"));
            }

            this.LifeImage = new BitmapImage(new Uri(@"pack://application:,,,/Resources/heart.png"));

            this.Shape = new GeometryGroup()
            {
                Children = new GeometryCollection(new Geometry[]
                    {
                        new RectangleGeometry(new Rect(-10, 0, 20, 62)),
                        new RectangleGeometry(new Rect(-(BODYWIDTH / 2), 62, 90, 40))
                    })
            };
        }    

        /// <summary>
        /// Gets or sets player's name property.
        /// </summary>
        /// <value>Player's name.</value>
        public string Name
        {
            get { return this.name; }
            set { this.name = value; }
        }

        /// <summary>
        /// Gets or sets the image of life.
        /// </summary>
        /// <value>Life image.</value>
        public BitmapImage LifeImage 
        {
            get { return this.lifeimage; }
            set { this.lifeimage = value; }
        }

        /// <summary>
        /// Gets or sets shield property.
        /// </summary>
        /// <value>The number of shields.</value>
        public int Shield 
        { 
            get { return this.shield; }
            set { this.shield = value; }
        }

        /// <summary>
        /// Gets or sets life property.
        /// </summary>
        /// <value>The number of life.</value>
        public int Life
        {
            get { return this.life; }
            set { this.life = value; } 
        }

        /// <summary>
        /// To move the player vertical and horizontal.
        /// </summary>
        /// <param name="speedX">Horizontal move.</param>
        /// <param name="speedY">Vertical move.</param>
        public void Fly(int speedX, int speedY)
        {
            this.X += speedX;
            this.Y += speedY;
        }
    }

    /// <summary>
    /// Big laser class.
    /// </summary>
    public class BigLaser : Component
    {
        /// <summary>
        /// Laser speed.
        /// </summary>
        private const int SPEED = -10;

        /// <summary>
        /// Laser width.
        /// </summary>
        private const int LASERWIDTH = 12;

        /// <summary>
        /// Laser height.
        /// </summary>
        private const int LASERHEIGHT = 30;

        /// <summary>
        /// Initializes a new instance of the <see cref="BigLaser"/> class.
        /// Constructor of big laser class.
        /// </summary>
        /// <param name="x">The start point's X coordinate.</param>
        /// <param name="y">The start point's Y coordinate.</param>
        public BigLaser(int x, int y)
        {
            this.Amount = 5;
            this.ComponentImage = new BitmapImage(new Uri(@"pack://application:,,,/Resources/red_laser.png"));
            this.Shape = new RectangleGeometry(new Rect(x - (LASERWIDTH / 2), y - (LASERHEIGHT / 2), LASERWIDTH, LASERHEIGHT));
        }    

        /// <summary>
        /// Gets or sets amount property.
        /// </summary>
        /// <value>The amount of big laser.</value>
        public int Amount { get; set; }

        /// <summary>
        /// Shoot one laser from the starting point.
        /// </summary>
        public void Shoot()
        {
            this.Y += SPEED;
        }
    }

    /// <summary>
    /// Laser class.
    /// </summary>
    public class Laser : Component
    {
        /// <summary>
        /// Laser speed.
        /// </summary>
        private const int SPEED = -25;

        /// <summary>
        /// Laser width.
        /// </summary>
        private const int LASERWIDTH = 9;

        /// <summary>
        /// Laser height.
        /// </summary>
        private const int LASERHEIGHT = 27;

        /// <summary>
        /// Initializes a new instance of the <see cref="Laser"/> class.
        /// Constructor of the laser class.
        /// </summary>
        /// <param name="x">The start point's X coordinate.</param>
        /// <param name="y">The start point's Y coordinate.</param>
        public Laser(int x, int y)
        {
            this.ComponentImage = new BitmapImage(new Uri(@"pack://application:,,,/Resources/red_laser.png"));
            this.Shape = new RectangleGeometry(new Rect(x - (LASERWIDTH / 2), y, LASERWIDTH, LASERHEIGHT));
        }  

        /// <summary>
        /// To shoot a laser.
        /// </summary>
        public void Shoot()
        {
            this.Y += SPEED;
        }  
    }

    /// <summary>
    /// Asteroids class.
    /// </summary>
    public class Asteroids : Component
    {
        /// <summary>
        /// Generate a random number to decide the image and size.
        /// </summary>
        private static Random rnd = new Random();

        /// <summary>
        /// Asteroid width.
        /// </summary>
        private int asteroidWidth = 60;

        /// <summary>
        /// Asteroid height.
        /// </summary>
        private int asterodiHeight = 80;

        /// <summary>
        /// Initializes a new instance of the <see cref="Asteroids"/> class.
        /// Constructor of Asteroid class.
        /// </summary>
        /// <param name="x">Random start point on X-axis. Y coordinate is always -130.</param>
        public Asteroids(int x)
        {
            this.Impact = false;
            this.AsteroidSelecter();
            this.Shape = new EllipseGeometry(new Rect(x, -130, this.asteroidWidth, this.asterodiHeight));
        }

        /// <summary>
        /// The speed is not constant because of the two types of fly.
        /// </summary>
        /// <param name="speedY">Vertical move.</param>
        public void FlyVertical(int speedY)
        {
            this.Y += speedY;
        }

        /// <summary>
        /// The asteroids fly cross the screen.
        /// </summary>
        /// <param name="speedX">Speed on X-axis .</param>
        /// <param name="speedY">Speed on Y-axis.</param>
        public void FlyCross(int speedX, int speedY)
        {
            this.X += speedX;
            this.Y += speedY;
        }

        /// <summary>
        /// Select the size of the asteroid.
        /// </summary>
        private void AsteroidSelecter()
        {
            int r = rnd.Next(0, 101);
            int randomAsteroid = rnd.Next(0, 101);
            if (r <= 25)
            {
                this.ImageSelecter(randomAsteroid);
            }
            else if (r > 25 && r <= 50)
            {
                this.ImageSelecter(randomAsteroid);
                this.asteroidWidth = 50;
                this.asterodiHeight = 50;
            }
            else if (r > 50 && r <= 75)
            {
                this.ImageSelecter(randomAsteroid);
            }
            else
            {
                this.ImageSelecter(randomAsteroid);
                this.asteroidWidth = 40;
                this.asterodiHeight = 60;
            }
        }

        /// <summary>
        /// Random image select.
        /// </summary>
        /// <param name="rndAsteroid">Select a random image to the asteroids.</param>
        private void ImageSelecter(int rndAsteroid)
        {
            if (rndAsteroid <= 25)
            {
                this.ComponentImage = new BitmapImage(new Uri(@"pack://application:,,,/Resources/Asteroid4.png"));
            }
            else if (rndAsteroid > 25 && rndAsteroid <= 50)
            {
                this.ComponentImage = new BitmapImage(new Uri(@"pack://application:,,,/Resources/Asteroid3.png"));
            }
            else if (rndAsteroid > 50 && rndAsteroid <= 75)
            {
                this.ComponentImage = new BitmapImage(new Uri(@"pack://application:,,,/Resources/Asteroid2.png"));
            }
            else
            {
                this.ComponentImage = new BitmapImage(new Uri(@"pack://application:,,,/Resources/Asteroid1.png"));
            }
        }   
    }

    /// <summary>
    /// Enemy Ship class.
    /// </summary>
    public class EnemySpaceship : Component
    {       
        /// <summary>
        /// Ship width.
        /// </summary>
        public const int ESPACESHIPWIDTH = 80;

        /// <summary>
        /// Ship height.
        /// </summary>
        public const int ESPACESHHIPHEIGHT = 92;

        /// <summary>
        /// Ship speed.
        /// </summary>
        private const int SPEED = 4;

        /// <summary>
        /// Initializes a new instance of the <see cref="EnemySpaceship"/> class.
        /// Constructor of Enemy Ships class.
        /// </summary>
        /// <param name="x">Random start point on X-axis.</param>
        public EnemySpaceship(int x)
        {
            this.Life = 2;
            this.Impact = false;
            this.Shape = new RectangleGeometry(new Rect(x, -130, ESPACESHIPWIDTH, ESPACESHHIPHEIGHT));
            this.ComponentImage = new BitmapImage(new Uri(@"pack://application:,,,/Resources/EnemySpaceship.png"));
        }

        /// <summary>
        /// Gets or sets life property.
        /// </summary>
        /// <value>Number of life.</value>
        public int Life { get; set; }

        /// <summary>
        /// Fly vertical.
        /// </summary>
        public void Fly()
        {
            this.Y += SPEED;
        }
    }

    /// <summary>
    /// Enemy laser class.
    /// </summary>
    public class EnemyLaser : Component
    {
        /// <summary>
        /// Laser speed.
        /// </summary>
        private const int SPEED = 8;

        /// <summary>
        /// Laser width.
        /// </summary>
        private const int ELASERWIDTH = 9;

        /// <summary>
        /// Laser height.
        /// </summary>
        private const int ELASERHEIGHT = 27;

        /// <summary>
        /// Initializes a new instance of the <see cref="EnemyLaser"/> class.
        /// Constructor of Enemy laser class.
        /// </summary>
        /// <param name="x">Random start point.It is actually the same start point of the Enemy Spaceship. The +38 is to start from the front edge of the ship.</param>
        public EnemyLaser(int x)
        {
            this.Impact = false;
            this.ComponentImage = new BitmapImage(new Uri(@"pack://application:,,,/Resources/Green_laser.png"));
            this.Shape = new RectangleGeometry(new Rect(x + 38, -50, ELASERWIDTH, ELASERHEIGHT));
        }  

        /// <summary>
        /// Fly vertical.
        /// </summary>
        public void Fly()
        {
            this.Y += SPEED;
        }  
    }

    /// <summary>
    /// Boss energy ball class.
    /// </summary>
    public class BossEnergyBall : Component
    {
        /// <summary>
        /// Ball speed.
        /// </summary>
        private const int SPEED = 6;

        /// <summary>
        /// Ball width.
        /// </summary>
        private const int LASERBALLWIDTH = 100;

        /// <summary>
        /// Initializes a new instance of the <see cref="BossEnergyBall"/> class.
        /// Constructor of BossEnergyBall class.
        /// </summary>
        /// <param name="x">The balls start point on X-axis.</param>
        /// <param name="y">The balls start point on Y-axis.</param>
        public BossEnergyBall(int x, int y)
        {
            this.ComponentImage = new BitmapImage(new Uri(@"pack://application:,,,/Resources/energyball.png"));
            EllipseGeometry bottom = new EllipseGeometry(new Rect(x, y, LASERBALLWIDTH, LASERBALLWIDTH));
            this.Shape = bottom;
        }

        /// <summary>
        /// Fly vertical.
        /// </summary>
        public void Fly()
        {
            this.Y += SPEED;
        }
    }

    /// <summary>
    /// BossLaser class.
    /// </summary>
    public class BossLaser : Component
    {
        /// <summary>
        /// Boss laser Speed.
        /// </summary>
        private const int SPEED = 9;

        /// <summary>
        /// Boss laser width.
        /// </summary>
        private const int LASERWIDTH = 80;

        /// <summary>
        /// Boss laser height.
        /// </summary>
        private const int LASERHEIGHT = 250;

        /// <summary>
        /// Initializes a new instance of the <see cref="BossLaser"/> class.
        /// Constructor of BossLaser class.
        /// </summary>
        /// <param name="x">The start point on X-axis.</param>
        /// <param name="y">The start point on Y-axis.</param>
        public BossLaser(int x, int y)
        {
            this.ComponentImage = new BitmapImage(new Uri(@"pack://application:,,,/Resources/Green_laser.png"));
            EllipseGeometry bottom = new EllipseGeometry(new Rect(x, y, LASERWIDTH, LASERHEIGHT));
            this.Shape = bottom;
        } 

        /// <summary>
        /// Fly vertical.
        /// </summary>
        public void Fly()
        {
            this.Y += SPEED;
        }  
    }

    /// <summary>
    /// Boss class.
    /// </summary>
    public class Boss : Component
    {
        /// <summary>
        /// Boss speed.
        /// </summary>
        private const int SPEED = 3;

        /// <summary>
        /// Boss width.
        /// </summary>
        private const int BOSSWIDTH = 600;

        /// <summary>
        /// Boss height.
        /// </summary>
        private const int BOSSHEIGHT = 600;

        /// <summary>
        /// Initializes a new instance of the <see cref="Boss"/> class.
        /// Constructor of Boss class.
        /// </summary>
        public Boss()
        {
            this.Life = 10000;
            this.ComponentImage = new BitmapImage(new Uri(@"pack://application:,,,/Resources/DeathStar.png"));
            this.Shape = new EllipseGeometry(new Rect(0, -650, BOSSWIDTH, BOSSHEIGHT));
        }

        /// <summary>
        /// Gets or sets life property.
        /// </summary>
        /// <value>The Boss's life.</value>
        public int Life { get; set; }

        /// <summary>
        /// Fly vertical.
        /// </summary>
        public void Fly()
        {
            this.Y += SPEED;
        }
    }

    /// <summary>
    /// Star class.
    /// </summary>
    public class Stars : Component
    {
        /// <summary>
        /// Random number generate.
        /// </summary>
        private static Random rnd = new Random();

        /// <summary>
        /// Star width.
        /// </summary>
        private int starWidth = 8;

        /// <summary>
        /// Star height.
        /// </summary>
        private int starHeight = 8;

        /// <summary>
        /// Initializes a new instance of the <see cref="Stars"/> class.
        /// Constructor of Stars class. Select a random image for the star.
        /// </summary>
        /// <param name="x">The start point on X-axis.</param>
        /// <param name="y">The start point on Y-axis.</param>
        public Stars(int x, int y)
        {
            int rndStar = rnd.Next(1, 100);
            if (rndStar < 30)
            {
                this.ComponentImage = new BitmapImage(new Uri(@"pack://application:,,,/Resources/star3.png"));
            }

            if (rndStar > 80)
            {
                this.ComponentImage = new BitmapImage(new Uri(@"pack://application:,,,/Resources/star2.png"));
                this.starHeight = 13;
                this.starWidth = 13;
            }

            if (rndStar > 40 && rndStar < 80)
            {
                this.ComponentImage = new BitmapImage(new Uri(@"pack://application:,,,/Resources/star1.png"));
            }

            this.Shape = new EllipseGeometry(new Rect(x, y, this.starWidth, this.starHeight));
        }

        /// <summary>
        /// Fly vertical.
        /// </summary>
        /// <param name="speed">The speed of stars.</param>
        public void Fly(int speed)
        {
            this.Y += speed;
        }
    }
}
