﻿namespace Kosztyu_adam_gg2hlc_beadando
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using System.Windows.Media;
    using System.Windows.Media.Imaging;

    /// <summary>
    /// View model class.
    /// </summary>
    public class ViewModel
    {
        /// <summary>
        /// Static view model.
        /// </summary>
        public static ViewModel VM;

        /// <summary>
        /// Constructor of the view model class. 
        /// Initializes a new instance of the <see cref="ViewModel"/> class.
        /// </summary>
        public ViewModel()
        {
            this.Multiplayer = false;
            this.Player1 = new Player(true);
            this.Player2 = null;
            this.FinalBoss = new Boss();
            this.Stars = new List<Stars>();
            this.Laser = new List<Laser>();
            this.Biglaser = new List<BigLaser>();
            this.Asteroids = new List<Asteroids>();
            this.Bosslaser = new List<BossLaser>();
            this.Enemyships = new List<EnemySpaceship>();
            this.Enemylaser = new List<EnemyLaser>();
            this.Bossenergyball = new List<BossEnergyBall>();
        }

        /// <summary>
        /// Gets or sets multiplayer property.
        /// </summary>
        /// <value>Logic value.</value>
        public bool Multiplayer { get; set; }

        /// <summary>
        /// Gets or sets Player1 property.
        /// </summary>
        /// <value>The Player's properties.</value>
        public Player Player1 { get; set; }

        /// <summary>
        /// Gets or sets Player2 property if multiplayer is true.
        /// </summary>
        /// <value>The second player's property.</value>
        public Player Player2 { get; set; }

        /// <summary>
        /// Gets or sets Boss property.
        /// </summary>
        /// <value>The Boss's property.</value>
        public Boss FinalBoss { get; set; }

        /// <summary>
        /// Gets or sets Stars property.
        /// </summary>
        /// <value>Stars property.</value>
        public List<Stars> Stars { get; set; }

        /// <summary>
        /// Gets or sets Laser property.
        /// </summary>
        /// <value>Laser property.</value>
        public List<Laser> Laser { get; set; }

        /// <summary>
        /// Gets or sets BigLaser property.
        /// </summary>
        /// <value>Big laser property.</value>
        public List<BigLaser> Biglaser { get; set; }

        /// <summary>
        /// Gets or sets asteroids property.
        /// </summary>
        /// <value>Asteroids property.</value>
        public List<Asteroids> Asteroids { get; set; }

        /// <summary>
        /// Gets or sets Boss laser property.
        /// </summary>
        /// <value>Boss laser property.</value>
        public List<BossLaser> Bosslaser { get; set; }

        /// <summary>
        /// Gets or sets enemy laser property.
        /// </summary>
        /// <value>Enemy laser property.</value>
        public List<EnemyLaser> Enemylaser { get; set; }

        /// <summary>
        /// Gets or sets Enemy ships property.
        /// </summary>
        /// <value>Enemy ships property.</value>
        public List<EnemySpaceship> Enemyships { get; set; }

        /// <summary>
        /// Gets or sets boss energy ball property.
        /// </summary>
        /// <value>Boss energy ball property.</value>
        public List<BossEnergyBall> Bossenergyball { get; set; }

        /// <summary>
        /// To get the View model from other classes.
        /// </summary>
        /// <returns>Returns the View model.</returns>
        public static ViewModel Get()
        {
            if (VM == null)
            {
                VM = new ViewModel();
            }

            if (VM.Multiplayer == true)
            {
                VM.Player2 = new Player(false);
            }
                
            return VM;           
        }

        /// <summary>
        /// To "clear" all components of the game.
        /// </summary>
        /// <param name="vm">Current view model.</param>
        public void Clear(ViewModel vm)
        {
            this.Multiplayer = false;
            this.Player1 = new Player(true);
            this.Player2 = null;
            this.FinalBoss = new Boss();
            this.Stars = new List<Stars>();
            this.Laser = new List<Laser>();
            this.Biglaser = new List<BigLaser>();
            this.Asteroids = new List<Asteroids>();
            this.Bosslaser = new List<BossLaser>();
            this.Enemyships = new List<EnemySpaceship>();
            this.Enemylaser = new List<EnemyLaser>();
            this.Bossenergyball = new List<BossEnergyBall>();
        }
    }
}
